from loguru import logger
from typing import List
import os


def is_command(line: str) -> bool:
    return len(line) > 0 and line[0] == "#"


def execute_command(command: str) -> None:
    commands = {
        "lock": lock
    }

    command_parts = command.strip()[1:].split(" ")
    command_function = command_parts[0]
    command_arguments = command_parts[1:]

    if command_function in commands:
        commands[command_function](command_arguments)


def lock(args: List[str]):
    logger.debug(f"lock: {str.join(' ', args)}")
    os.system(f'C:\\Users\\janpe\\source\\rescuebutton\\RescueButton\\bin\\Debug\\RescueButton.exe --lock --unlock --lockfile C:\\Users\\janpe\\lock.txt {str.join(" ", args)}')

