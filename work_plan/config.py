import os
import toml


def get_config():
    config_path = 'work_plan/config.toml'

    config = {}
    if os.path.exists(config_path):
        config = toml.load(config_path)

    return config


config = get_config()