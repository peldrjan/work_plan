import argparse
import datetime
import glob
import os
from time import sleep

from loguru import logger
from playsound import playsound
import gui
from resources.sounds import no_plan_alarm, alarm
from commands import is_command, execute_command
from config import config
import threading
import time

from sys import platform
if platform == "win32":
    import ctypes


def playsound_async(sound):
    threading.Thread(target=playsound, args=[sound]).start()


def parse_arguments():
    parser = argparse.ArgumentParser(description='Work plan')
    parser.add_argument('--plan_folder', required=False, type=str,
                        help="Folder with plan files (default value - current working directory)")
    return parser.parse_args()


def main():
    args = parse_arguments()
    if args.plan_folder:
        config['plan_folder'] = args.plan_folder
    if 'plan_folder' not in config:
        config['plan_folder'] = os.getcwd()

    shown_times = []

    logger.info(f"Work plan started in folder {config['plan_folder']}")

    while True:
        plan_file, error_message = find_plan_file(config['plan_folder'])

        if plan_file is None:
            undefined_lock()
        else:
            time_with_activity = get_activities(plan_file)

            now = datetime.datetime.now()
            from_time, to_time = get_current_time_range(time_with_activity.keys(), now)


            if from_time is None:
                undefined_lock()

            elif to_time is None:
                undefined_lock()

            elif (to_time - from_time).total_seconds() > 60 * 60:
                undefined_lock()

            elif from_time not in shown_times:
                logger.info(f"{from_time.strftime('%H:%M')} - {to_time.strftime('%H:%M')}: {time_with_activity[from_time]}")

                shown_times.append(from_time)
                playsound_async(alarm)
                gui.alert(f"{from_time.strftime('%H:%M')} - {to_time.strftime('%H:%M')}", time_with_activity[from_time],
                          timeout=None)
                if is_command(time_with_activity[from_time]):
                    execute_command(time_with_activity[from_time])

                sleep(5)

            else:
                sleep(5)


def undefined_lock():
    while True:
        minute = datetime.datetime.now().minute
        hour = datetime.datetime.now().hour
        if ((28 < minute < 30) or minute > 58):
            print(f'Lock as {datetime.datetime.now()}')
            if platform == "linux" or platform == "linux2":
                os.system('xdg-screensaver lock')
            elif platform == "win32":
                ctypes.windll.user32.LockWorkStation()
            time.sleep(1)
        else:
            break
    time.sleep(1)

def get_current_time_range(activity_times, now):
    lesser_equal = None
    greater = None

    for time in activity_times:
        if time <= now and (lesser_equal is None or time > lesser_equal):
            lesser_equal = time
        elif now < time and (greater is None or greater > time):
            greater = time

    return lesser_equal, greater


def get_activities(plan_file):
    date = os.path.splitext(os.path.basename(plan_file))[0]

    activities = open(plan_file, 'r', encoding="utf8").readlines()
    time_with_activity_str = {line[0]: " - ".join(line[1:]) for line in
                          [line.strip().split(" - ") for line in activities if line.strip() != ""]
                          if len(line) >= 2}

    time_with_activity = {}
    for time_str, activity in time_with_activity_str.items():
        try:
            time = datetime.datetime.strptime(f"{date}T{time_str}", "%Y-%m-%dT%H:%M")
            time_with_activity[time] = activity
        except Exception as error:
            pass
            easygui.msgbox(str(error), title="Error")

    return time_with_activity


def find_plan_file(plan_directory):
    plan_files = glob.glob(f"{plan_directory}/**/*.txt", recursive=True)

    matching_files = [file for file in plan_files if
                     f"{datetime.datetime.now().date()}.txt" in file]

    result_plan_file = None
    error_message = None

    if len(matching_files) == 1:
        result_plan_file = matching_files[0]
    elif len(matching_files) > 1:
        error_message = f"Multiple plan files for today!\n{matching_files}"
    else:
        error_message = "No plan file for today"
    return result_plan_file, error_message


if __name__ == '__main__':
    main()
